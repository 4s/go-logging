package logging

import (
	"context"
	"fmt"
	"os"

	"github.com/onrik/logrus/filename"
	"github.com/sirupsen/logrus"
)

type loggerKeyType int

const loggerKey loggerKeyType = iota

var loggerName string
var logger *logrus.Entry

func init() {
	var log = logrus.New()

	formatter := &logrus.JSONFormatter{
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyTime:  "@timestamp",
			logrus.FieldKeyLevel: "level",
			logrus.FieldKeyMsg:   "message",
		},
	}
	log.Formatter = formatter
	loggerName = os.Getenv("LOGGER_NAME")
	logLevel, err := logrus.ParseLevel(os.Getenv("LOG_LEVEL_DK_S4"))
	if err != nil {
		fmt.Println(err)
		logLevel = logrus.DebugLevel
	}

	log.SetLevel(logLevel)

	filenameHook := filename.NewHook()
	filenameHook.Field = "class"
	log.AddHook(filenameHook)
	contextLogger := log.WithFields(logrus.Fields{
		"hostname":    os.Getenv("HOSTNAME"),
		"logger_name": loggerName,
	})
	logger = contextLogger
	WithContext(nil).Infof("log level set to %v", logLevel)
}

// NewContext returns a context that has a logger with the extra fields added
func NewContext(ctx context.Context, fields logrus.Fields) context.Context {
	return context.WithValue(ctx, loggerKey, WithContext(ctx).WithFields(fields))
}

// WithContext returns a logger with as much context as possible
func WithContext(ctx context.Context) *logrus.Entry {
	if ctx == nil {
		return logger
	}
	ctxLogger, ok := ctx.Value(loggerKey).(*logrus.Entry)
	if !ok {
		return logger
	}
	return ctxLogger
}
