# Go Logging library

A library that supports the formatting of log messages to fit with the 4S-microservice logging style.

This library can be used in collaboration with tools like fluentd and Kibana.

## Project wiki
See: [Main.md](docs//Main.md)

## Project status
4S maturity level: prototyping.

## Prerequisites
You need to have the following software installed

Go (golang) (https://golang.org/)
librdkafka (https://github.com/confluentinc/confluent-kafka-go#installing-librdkafka)

## Building:

This library does not contain a main package, and therefore cannot be built on its own.

## Usage

To use this library, it must be included in a project containing a main-package.

To include this library run the following command in your terminal/shell:

```bash
go get "bitbucket.org/4s/go-logging"
```

After retrieving the library, the library can be included by importing it, in the files in which it is required:

```golang
package ...
import (
  ...
  log "bitbucket.org/4s/go-logging"
)
...
ctx := context.Background()
log.WithContext(ctx).Infof("%v started", serviceName)
...
```

## Environment variables
The files `service.env` contains the environment variables that you can use if you include and use this 
library in you own services.

`LOG_LEVEL_DK_S4` specifies the log level of the service. 
The available log levels are (in descending order of severity): 

`panic`  
`fatal`  
`error`  
`warning`  
`info`  
`debug`  
`trace`

`LOGGER_NAME` is the name of the logger. This will be added as a field ("logger_name") in the output from each log-statement