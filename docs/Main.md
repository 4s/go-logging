# Go Logging library

A library that supports the formatting of log messages to fit with the 4S-microservice logging style.

This library can be used in collaboration with tools like fluentd and Kibana.

## Usage

To be able to keep track of the context in which a message is logged, an object of the type context.Context is needed:

```golang
package ...
import (
  ...
  log "bitbucket.org/4s/go-logging"
)
...
ctx := context.Background()
log.WithContext(ctx).Infof("%v started", serviceName)
...
```

This will log the provided message with the standard fields. These fields are as follows:

| Field       | Value                                                                 |
|-------------|:----------------------------------------------------------------------|
| @timestamp  | a timestamp indicating at what time the message was logged            |
| class       | the name of the file and linenumber from where the message was logged |
| hostname    | the name of the host. If running in a docker container, this will be appended automatically. Otherwise, this can be manually specified using the environment-variable `HOSTNAME`.  |
| level       | the level of the log entry                                            |
| logger_name | the name of the logger                                                |
| message     | the message that was logged                                           |

To append additional fields the method NewContext can be used:

```golang
package ...
import (
  ...
  log "bitbucket.org/4s/go-logging"
)
...
ctx := context.Background()
ctx = log.NewContext(map[string]interface{}{
  "additionalField": "value",
  "anotherAdditionalField": "value",
})
log.WithContext(ctx).Infof("%v started", serviceName)
...
```
